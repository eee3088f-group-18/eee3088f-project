/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "stm32f0xx_hal.h"
#include "shtc3.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define EEPROM_ADDR 0xA0
#define EEPROM_MAX_BYTES 32768
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;

I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
uint16_t lightLevel;
uint32_t temperature;
uint32_t humidity;
uint16_t writeMemAddr;
uint16_t readMemAddr;
uint8_t ftdiRead;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

//UART receive complete callback
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (ftdiRead == '\r') {
		if (readMemAddr+6 <= writeMemAddr) {
			uint8_t light[2];
			uint8_t temp[2];
			uint8_t hum[2];
			HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDR, readMemAddr, 2, &light, 2, HAL_MAX_DELAY);
			HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDR, readMemAddr+2, 2, &temp, 2, HAL_MAX_DELAY);
			HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDR, readMemAddr+4, 2, &hum, 2, HAL_MAX_DELAY);
			char data[30];
			memset(data, 0, sizeof(data));
			sprintf(data, "%u %02x%02x %02x%02x %02x%02x", ((readMemAddr-2)/6), light[1], light[0], temp[1], temp[0], hum[1], hum[0]);
			HAL_UART_Transmit(&huart1, &data, strlen(data), HAL_MAX_DELAY);
			readMemAddr += 6;
			char newline[2] = "\r\n";
			HAL_UART_Transmit(&huart1, &newline, 2, HAL_MAX_DELAY);
		}
		else {
			char data[15] = "\r\nEnd\r\n";
			HAL_UART_Transmit(&huart1, &data, strlen(data), HAL_MAX_DELAY);
		}
	}
	else if (ftdiRead == 'r') {
		readMemAddr = 2;
		char data[15] = "\r\nRestart\r\n";
		HAL_UART_Transmit(&huart1, &data, strlen(data), HAL_MAX_DELAY);
	}
	else if (ftdiRead == 'c') {
		readMemAddr = 2;
		writeMemAddr = 2;
		HAL_I2C_Mem_Write(&hi2c1, EEPROM_ADDR, 0, 2, &writeMemAddr, 2, HAL_MAX_DELAY);
		char data[15] = "\r\nClear\r\n";
		HAL_UART_Transmit(&huart1, &data, strlen(data), HAL_MAX_DELAY);
	}
	else if (ftdiRead == 'a') {
		while (readMemAddr+6 <= writeMemAddr) {
			uint8_t light[2];
			uint8_t temp[2];
			uint8_t hum[2];
			HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDR, readMemAddr, 2, &light, 2, HAL_MAX_DELAY);
			HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDR, readMemAddr+2, 2, &temp, 2, HAL_MAX_DELAY);
			HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDR, readMemAddr+4, 2, &hum, 2, HAL_MAX_DELAY);
			char data[30];
			memset(data, 0, sizeof(data));
			sprintf(data, "%u %02x%02x %02x%02x %02x%02x\r\n", ((readMemAddr-2)/6), light[1], light[0], temp[1], temp[0], hum[1], hum[0]);
			HAL_UART_Transmit(&huart1, &data, strlen(data), HAL_MAX_DELAY);
			readMemAddr += 6;
		}
	}
	else if (ftdiRead == 'm') {
		uint8_t light[2];
		uint8_t temp[2];
		uint8_t hum[2];
		light[0] = (uint8_t)(lightLevel && 0xFF);
		light[1] = (uint8_t)(lightLevel >> 8);
		temp[0] = (uint8_t)(temperature && 0xFF);
		temp[1] = (uint8_t)((temperature >> 8) && 0xFF);
		hum[0] = (uint8_t)(humidity && 0xFF);
		hum[1] = (uint8_t)((humidity >> 8) && 0xFF);
		char data[30];
		memset(data, 0, sizeof(data));
		sprintf(data, "%02x%02x %02x%02x %02x%02x\r\n", light[1], light[0], temp[1], temp[0], hum[1], hum[0]);
		HAL_UART_Transmit(&huart1, &data, strlen(data), HAL_MAX_DELAY);
	}
	HAL_UART_Receive_IT(&huart1, &ftdiRead, 1);
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
  HAL_UART_Receive_IT(&huart1 , &ftdiRead, 1);
  char detect[14] = "USB detected\r\n";
  HAL_UART_Transmit(&huart1, &detect, 13, 100);
  readMemAddr = 2;
  writeMemAddr = 2;
  //HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDR, 0, 2, &writeMemAddr, 2, HAL_MAX_DELAY);
  //if (writeMemAddr == 0) writeMemAddr = 2;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	if (writeMemAddr+6 < EEPROM_MAX_BYTES) {
		// Analog reading
		HAL_ADC_Start(&hadc);
		HAL_ADC_PollForConversion(&hadc, 100);
		lightLevel = HAL_ADC_GetValue(&hadc);
		HAL_Delay(100);
		// Digital reading
		shtc3_wakeup(&hi2c1);
		HAL_Delay(5);
		shtc3_perform_measurements(&hi2c1, &temperature, &humidity);
		HAL_Delay(5);
		shtc3_sleep(&hi2c1);
		HAL_Delay(100);
		// Write to EEPROM
		HAL_I2C_Mem_Write(&hi2c1, EEPROM_ADDR, writeMemAddr, 2, &lightLevel, 2, HAL_MAX_DELAY);
		HAL_Delay(5);
		HAL_I2C_Mem_Write(&hi2c1, EEPROM_ADDR, writeMemAddr+2, 2, &temperature, 2, HAL_MAX_DELAY);
		HAL_Delay(5);
		//uint16_t test;
		//HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDR, writeMemAddr+2, 2, &test, 2, HAL_MAX_DELAY);
		HAL_I2C_Mem_Write(&hi2c1, EEPROM_ADDR, writeMemAddr+4, 2, &humidity, 2, HAL_MAX_DELAY);
		HAL_Delay(5);
		writeMemAddr += 6;
		HAL_I2C_Mem_Write(&hi2c1, EEPROM_ADDR, 0, 2, &writeMemAddr, 2, HAL_MAX_DELAY);
		HAL_Delay(100);

	}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI14|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL2;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_SYSCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_7;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */

  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00303D5B;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_AUTOBAUDRATE_INIT;
  huart1.AdvancedInit.AutoBaudRateEnable = UART_ADVFEATURE_AUTOBAUDRATE_ENABLE;
  huart1.AdvancedInit.AutoBaudRateMode = UART_ADVFEATURE_AUTOBAUDRATE_ONSTARTBIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
