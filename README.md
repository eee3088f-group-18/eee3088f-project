# EEE3088F Group 18 Project

## Description
This project is for the development and design of a HAT (Hardware Attached on Top) for EEE3088F 2023. The designed PCB will be able to sense the environmental parameters of light, temperature and humidity. These parameters will be processed and output to perform a range of useful functions such as environmental monitoring and automation of environmental control.

## Structure
* PCB: The KiCAD Project, Schematics & Layout and Project Libraries
* Firmware: Any software developed for the hardware or instructions on where to find relevant sofware
* Documents: Project documentation and component datasheets
* Production: The gerber files, BOM, Budget, or anything required by the fabrication houses
* Simulation: Any simulation files (eg SPICE) or design stage generated results (Eg matlab or excel) 
* CAD: Any 3D models or mechanical designs for enclosures or support

## Installation
Software developed for the hardware or instructions on where to find relevant sofware can be found in the [Firmware](https://gitlab.com/eee3088f-group-18/eee3088f-project/-/tree/main/Firmware) directory.

## Usage
The project documentation can be found in the [Documents](https://gitlab.com/eee3088f-group-18/eee3088f-project/-/tree/main/Documents) directory and the files required to produce the PCB can be found in the [Production](https://gitlab.com/eee3088f-group-18/eee3088f-project/-/tree/main/Production) directory.

## Contact
* Tilal Mukhtar - MKHTIL001@myuct.ac.za
* Deereshan Naidoo - NDXDEE010@myuct.ac.za
* Dylan Trowsdale - TRWDYL001@myuct.ac.za

## Project Status and Roadmap
This project is actively under developement.

## Authors and Acknowledgement
Authors:
* Tilal Mukhtar
* Deereshan Naidoo
* Dylan Trowsdale

## License
[Creative Commons Attribution 4.0 International license](https://choosealicense.com/licenses/cc-by-4.0/)
